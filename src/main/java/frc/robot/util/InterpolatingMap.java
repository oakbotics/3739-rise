package frc.robot.util;

import java.util.TreeMap;
import java.util.Map.Entry;

public class InterpolatingMap {
    
    private TreeMap<Double, RPMValue> m_values;

    public InterpolatingMap(TreeMap<Double, RPMValue> values) {
        m_values = values;
    }

    public RPMValue getInterpolated(double key) {
        System.out.println("Get At: " + key);
        Entry<Double, RPMValue> above, below;
        try {
            above = m_values.ceilingEntry(key);
        }
        catch (NullPointerException e) {
            return m_values.floorEntry(key).getValue();
        }
        try {
            below = m_values.floorEntry(key);
        }
        catch (NullPointerException e) {
            return m_values.ceilingEntry(key).getValue();
        }

        double tx = above.getKey() - below.getKey();
        double dydxt = (above.getValue().topRPM() - below.getValue().topRPM()) / tx;
        double dydxb = (above.getValue().bottomRPM() - below.getValue().bottomRPM()) / tx;

        System.out.println("Below k: " + below.getKey() +  " t: " + below.getValue().topRPM() + " b: " + below.getValue().bottomRPM());
        System.out.println("Above k: " + above.getKey() +  " t: " + above.getValue().topRPM() + " b: " + above.getValue().bottomRPM());
        
        double dx = key - below.getKey();

        System.out.println("Tx: " + tx + " SlopeT : " + dydxt + " SlopeB: " + dydxb + " Dx: " + dx);
        
        return new RPMValue(below.getValue().topRPM() + dydxt * dx, below.getValue().bottomRPM() + dydxb * dx);
    }
}