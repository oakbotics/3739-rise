package frc.robot.util;

public class RPMValue {
    private final double m_topRPM, m_bottomRPM;

    public RPMValue(double topRPM, double bottomRPM) {
        m_topRPM = topRPM;
        m_bottomRPM = bottomRPM;
    }
    
    public double topRPM() {
        return m_topRPM;
    }

    public double bottomRPM() {
        return m_bottomRPM;
    }
}
