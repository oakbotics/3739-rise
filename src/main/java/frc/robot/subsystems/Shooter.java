package frc.robot.subsystems;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.TreeMap;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.DemandType;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.VelocityMeasPeriod;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.Constants;
import frc.robot.util.InterpolatingMap;
import frc.robot.util.RPMValue;

public class Shooter extends SubsystemBase {
    public static class DistanceRPMPoint {
        @JsonProperty("distance")
        public double distance;

        @JsonProperty("topRPM")
        public double topRPM;

        @JsonProperty("bottomRPM")
        public double bottomRPM;
    }

    private TalonSRX m_mtrTopA;
    private TalonSRX m_mtrBottomA;
    private VictorSPX m_mtrBottomB;

    private TalonSRXConfiguration bottomConfig;
    private TalonSRXConfiguration topConfig;
    private InterpolatingMap m_rpmDistanceMap;
    
    private final double kS_Top = 1.11, kV_Top = 0.0841, kA_Top = 0.0216;
    private final double kS_Bottom = 0.566, kV_Bottom = 0.0834, kA_Bottom = 0.0841;
    private double kP_Top = 0.199, kD_Top = 2.05, kP_Bottom = 0.434, kD_Bottom = 0.185;

    private final double kRPMPerEncoderTicks = 1;

    private double kOutputTrim = 1.0, kTopScale = 1.0;

    private SimpleMotorFeedforward m_topFF;
    private SimpleMotorFeedforward m_bottomFF;
    
    public Shooter() {
        m_topFF = new SimpleMotorFeedforward(kS_Top, kV_Top, kA_Top);
        m_bottomFF = new SimpleMotorFeedforward(kS_Bottom, kV_Bottom, kA_Bottom);

        m_mtrTopA = new TalonSRX(Constants.CAN_ID_MTR_SHOOTER_TOPA);
        m_mtrBottomA = new TalonSRX(Constants.CAN_ID_MTR_SHOOTER_BOTTOMA);
        m_mtrBottomB = new VictorSPX(Constants.CAN_ID_MTR_SHOOTER_BOTTOMB);

        m_mtrTopA.configFactoryDefault();
        m_mtrBottomA.configFactoryDefault();
        m_mtrBottomB.configFactoryDefault();

        topConfig = new TalonSRXConfiguration();
        topConfig.primaryPID.selectedFeedbackSensor = FeedbackDevice.CTRE_MagEncoder_Relative;
        topConfig.slot0.kP = kP_Top;
        topConfig.slot0.kD = kD_Top;
        topConfig.velocityMeasurementPeriod = VelocityMeasPeriod.Period_100Ms;
        topConfig.velocityMeasurementWindow = 20;

        bottomConfig = new TalonSRXConfiguration();
        bottomConfig.primaryPID.selectedFeedbackSensor = FeedbackDevice.CTRE_MagEncoder_Relative;
        bottomConfig.slot0.kP = kP_Bottom;
        bottomConfig.slot0.kD = kD_Bottom;
        bottomConfig.velocityMeasurementPeriod = VelocityMeasPeriod.Period_100Ms;
        bottomConfig.velocityMeasurementWindow = 20;

        m_mtrTopA.configAllSettings(topConfig);
        m_mtrBottomA.configAllSettings(bottomConfig);
        
        m_mtrTopA.setSensorPhase(false);
        m_mtrBottomA.setSensorPhase(false);

        
        m_mtrTopA.setInverted(InvertType.None);
        m_mtrBottomA.setInverted(InvertType.None);
        m_mtrBottomB.setInverted(InvertType.FollowMaster);
        m_mtrBottomB.follow(m_mtrBottomA); 


        m_rpmDistanceMap = loadRpmDistanceMap("shooter_distances.json");
    }

    private static InterpolatingMap loadRpmDistanceMap(String filePath) {
        try (BufferedReader fReader = Files.newBufferedReader(Filesystem.getDeployDirectory().toPath().resolve(filePath))) {
            ObjectReader jsonParser = new ObjectMapper().readerFor(DistanceRPMPoint[].class);

            DistanceRPMPoint[] distanceRPMs = jsonParser.readValue(fReader);

            TreeMap<Double, RPMValue> distanceRPMMap = new TreeMap<>();
            for (var pt : distanceRPMs) {
                distanceRPMMap.put(pt.distance, new RPMValue(pt.topRPM, pt.bottomRPM));
                System.out.println("Dist: " + pt.distance + "  Top: " + pt.topRPM + "  Bottom: " + pt.bottomRPM);
            }

            return new InterpolatingMap(distanceRPMMap);
        } catch (IOException ex) {
            DriverStation.reportError("Error loading distance -> rpm map. Check the file", ex.getStackTrace());
            
            SmartDashboard.putBoolean("Shooter Fault", true);
            
            return null;
        }
    }

    @Override
    public void periodic() {
    }

    public void setFlywheelSpeeds(double topRPM, double bottomRPM) {
        System.out.println(topRPM + " : " + bottomRPM);
        m_mtrTopA.set(ControlMode.Velocity, topRPM, DemandType.ArbitraryFeedForward, m_topFF.calculate(10.0 * topRPM / (4096.0)) / 12.0);
        m_mtrBottomA.set(ControlMode.Velocity, bottomRPM, DemandType.ArbitraryFeedForward, m_bottomFF.calculate(10.0 * bottomRPM / (4096.0)) / 12.0);
    }
    
    public void setShooterPower(double speedTopA, double speedBottomB) {
        m_mtrTopA.set(ControlMode.PercentOutput, speedTopA);
        m_mtrBottomA.set(ControlMode.PercentOutput, speedBottomB);
    }

    public void shootToDistance(double distance) {
        if (m_rpmDistanceMap == null) {
            this.setShooterPower(kTopScale * kOutputTrim * 0.75, kOutputTrim * 0.75);
        }
        else {
            RPMValue rpmVal = m_rpmDistanceMap.getInterpolated(distance);
            this.setFlywheelSpeeds(
                kOutputTrim * rpmVal.topRPM(), 
                kOutputTrim * rpmVal.bottomRPM()
            );
        }
    }

    public boolean rpmOnTarget(double rpmThreshold) {
        return 
            Math.abs(m_mtrTopA.getClosedLoopError() * kRPMPerEncoderTicks) < rpmThreshold && 
            Math.abs(m_mtrBottomA.getClosedLoopError() * kRPMPerEncoderTicks) < rpmThreshold;
    }

    public void stop() {
        this.setShooterPower(0, 0);
    }
}