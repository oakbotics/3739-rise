package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.Faults;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.TalonSRXConfiguration;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class RollerArms extends SubsystemBase {
    public static enum RollerArmsLocation {
        kFront,
        kBack
    }

    // real degrees per encoder units
    // degPerRev / ticksPerRev
    // 360.0 / (gearDown * encoderCPR)
    private final static double kDegreesPerEncoderTick = 360.0 / ((44.0 / 16.0) * 4096.0);

    // encoder units per 100ms
    // degPer100ms * ticksPerDeg
    // (30 degPerS / 10) / degPerTick
    private final static double kCruiseVelocity = (30.0 * 10.0) / kDegreesPerEncoderTick;

    // encoder units per 100ms per 100ms
    // degPer100msPer100ms * ticksPerDeg
    // (45 degPerSPerS / 10) / degPerTick
    private final static double kMaxAcceleration = (45.0 * 10.0) / kDegreesPerEncoderTick;

    private final static double kP = 1, kI = 0, kD = 0.0001;

    private final static double[] kARM_SETPOINTS = { 0.0, -116.0 };
    private int m_setPointIdx = 0;

    private TalonSRX m_rollerArms;
    
    private double m_armSetPoint;
    
    public RollerArms(RollerArmsLocation location) {
        if (location == RollerArmsLocation.kFront) {
            setName("Front Arms");
            m_rollerArms = new TalonSRX(Constants.CAN_ID_MTR_ROLLER_ARMS_FRONT);
        }
        else {
            setName("Back Arms");
            m_rollerArms = new TalonSRX(Constants.CAN_ID_MTR_ROLLER_ARMS_BACK);
        }

        m_rollerArms.configFactoryDefault();

        TalonSRXConfiguration config = new TalonSRXConfiguration();
        config.primaryPID.selectedFeedbackSensor = FeedbackDevice.CTRE_MagEncoder_Absolute;
        config.motionCruiseVelocity = (int)kCruiseVelocity;
        config.motionAcceleration = (int)kMaxAcceleration;
        config.motionCurveStrength = 1;
        config.slot0.closedLoopPeakOutput = 0.5;
        config.slot0.kP = kP;
        config.slot0.kI = kI;
        config.slot0.kD = kD;

        m_rollerArms.configAllSettings(config);
        m_rollerArms.setNeutralMode(NeutralMode.Brake);
        zeroRollerSensor();
    }

    @Override
    public void periodic() {
        Faults faults = new Faults();

        m_rollerArms.getFaults(faults);

        if (faults.HardwareFailure || faults.ResetDuringEn || faults.SensorOverflow || faults.UnderVoltage) {
            DriverStation.reportError(getName() + " - Talon SRX Failure " + faults.toString(), false);
            SmartDashboard.putBoolean(getName() + " Fault", true);
            m_rollerArms.clearStickyFaults();
        }
    }

    public void toggleArmSetpoint(boolean isDirectionUp) {
        if (isDirectionUp) {
            if (m_setPointIdx == kARM_SETPOINTS.length - 1) m_setPointIdx = 0;
            else m_setPointIdx++;
        }
        else {
            if (m_setPointIdx == 0) m_setPointIdx = kARM_SETPOINTS.length - 1;
            else m_setPointIdx--;
        }
        m_armSetPoint = kARM_SETPOINTS[m_setPointIdx];
        m_rollerArms.set(ControlMode.MotionMagic, m_armSetPoint / kDegreesPerEncoderTick);
    }

    public void zeroRollerSensor() {
        m_rollerArms.setSelectedSensorPosition(0);
    }

    public double getArmPosition() {
        return m_rollerArms.getSelectedSensorPosition() * kDegreesPerEncoderTick;
    }

    public void setSetPoint(double setPoint) {
        m_armSetPoint = setPoint;
        
        // Clamp range
        if (m_armSetPoint > 0.0) m_armSetPoint = 0.0;
        else if (m_armSetPoint < -130.0) m_armSetPoint = -130.0;

        m_rollerArms.set(ControlMode.MotionMagic, m_armSetPoint / kDegreesPerEncoderTick);
    }

    public void setRelativeSetpoint(double setpoint) {
        setSetPoint(setpoint + m_armSetPoint);
    }

    public void holdPosition() {
        m_rollerArms.set(ControlMode.MotionMagic, m_armSetPoint / kDegreesPerEncoderTick);
    }

    public void setPower(double speed) {
        m_rollerArms.set(ControlMode.PercentOutput, speed);
    }
    
    public void setArmSetIndex(int index) {
        index = index < 0 || index > kARM_SETPOINTS.length - 1 ? 0 : index;
        m_setPointIdx = index;
    }

    public double getSetPoint() {
        return m_armSetPoint;
    }

    public void stop() {
        m_rollerArms.set(ControlMode.PercentOutput, 0);
    }
}