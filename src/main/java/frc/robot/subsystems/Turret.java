package frc.robot.subsystems;

import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class Turret extends SubsystemBase {
    
    private CANSparkMax m_turretMotor;
    private CANEncoder m_encoder;
    private PIDController m_pidController;
    private DigitalInput m_leftLimit, m_rightLimit;

    private double m_currentSetpoint;

    private final double kP = 0.03, kD = 0.001;

    public Turret() {
        m_turretMotor = new CANSparkMax(Constants.CAN_ID_MTR_TURRET, MotorType.kBrushless);
        m_turretMotor.setInverted(true);
        m_turretMotor.setSmartCurrentLimit(20);
        m_encoder = m_turretMotor.getEncoder();
        m_pidController = new PIDController(kP, 0, kD);
        m_pidController.enableContinuousInput(-360, 360);
        m_leftLimit = new DigitalInput(1);
        m_rightLimit = new DigitalInput(0);

        resetEncoder();
        m_encoder.setPositionConversionFactor(1.0 / 105.0 * 360.0);
        m_pidController.setTolerance(2.0);
    }

    public void periodic() {
        // SmartDashboard.putNumber("Tur Set", m_currentSetpoint);
        // SmartDashboard.putNumber("Tur Act", getCurrentAngle());
        short faults = m_turretMotor.getFaults();
        if (faults != 0) {
            DriverStation.reportError("Turret - SparkMax Failure. bit field: " + faults, false);
            SmartDashboard.putBoolean("Turret Fault", true);
            m_turretMotor.clearFaults();
        }
    }

    public DigitalInput getLeftLimit() {
        return m_leftLimit;
    }

    public DigitalInput getRightLimit() {
        return m_rightLimit;
    }

    public double getCurrentAngle() {
        return m_encoder.getPosition();
    }
    
    public void setPower(double speed) {
        m_turretMotor.set(speed);
    }

    public boolean pidOnTarget() {
        return m_pidController.atSetpoint();
    }

    public void resetEncoder() {
        m_encoder.setPosition(0);
        m_currentSetpoint = 0;
    }

    public void setRelativeSetpoint(double angle) {
        setAbsoluteSetpoint(getCurrentAngle() + angle);
    }

    public void setAbsoluteSetpoint(double angle) {
        if (angle >= 105.0) angle = 105.0;
        else if (angle <= -105.0) angle = -105.0;
        m_currentSetpoint = angle;
    }

    public void holdSetpoint() {
        double output = m_pidController.calculate(m_encoder.getPosition(), m_currentSetpoint);
        m_turretMotor.set(output);
    }

    public double getCurrentSetpoint() {
        return m_currentSetpoint;
    }

    public void stop() {
        m_turretMotor.set(0);
    }
} 