package frc.robot.subsystems;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.geometry.Translation2d;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class VisionSystem extends SubsystemBase {
    private NetworkTable m_limelightTable;

    private final static double h = 98.25 - 37.75;
    private final static Rotation2d yOffset = new Rotation2d(Units.degreesToRadians(-22.0));

    private long tlLastChange = 0;

    public VisionSystem() {
        m_limelightTable = NetworkTableInstance.getDefault().getTable("limelight");
    }

    @Override
    public void periodic() {
        long tlCurChange = m_limelightTable.getEntry("tl").getLastChange();
        SmartDashboard.putBoolean("Limelight Connected", tlCurChange != tlLastChange);
        SmartDashboard.putNumber("Distance", getLemonLaunchDistance());
        tlLastChange = tlCurChange;
    }

    public void setTargeting(boolean isOn) {
        if (isOn) {
            m_limelightTable.getEntry("pipeline").setNumber(1);
        }
        else {
            m_limelightTable.getEntry("pipeline").setNumber(0);
        }
    }

    public void toggleCameraMode(boolean isDriverCamera) {
        if (isDriverCamera) {
            m_limelightTable.getEntry("camMode").setNumber(1);
            m_limelightTable.getEntry("ledMode").setNumber(1);
        } 
        else { 
            m_limelightTable.getEntry("camMode").setNumber(0);
            m_limelightTable.getEntry("ledMode").setNumber(0);
        }
    }

    public double getHorizontalAngle() {
        return m_limelightTable.getEntry("tx").getDouble(0);
    }

    public double tx() {
        return m_limelightTable.getEntry("tx").getDouble(0);
    }

    public double ty() {
        return m_limelightTable.getEntry("tx").getDouble(0);
    }

    // 3d vector trig distance
    public double getDistance3d() {
        
        // Unit vector outwards and up to the target
        Translation2d dyToTarget = new Translation2d(1.0, Math.tan(Units.degreesToRadians(ty()))).rotateBy(yOffset);
        // Horizontal component - ll x
        double x = Math.tan(Units.degreesToRadians(tx()));
        // Vertical component - ll y
        double y = dyToTarget.getY();
        // In-Outward component - 1
        double z = dyToTarget.getX();

        double scale = h / y;
        return Math.hypot(x, z) * scale;
    }

    // Simple trig distance
    public double getLemonLaunchDistance() {
        return h / Math.tan(Units.degreesToRadians(m_limelightTable.getEntry("ty").getDouble(0) + 22.0));
    }

    public boolean hasTarget() {
        return m_limelightTable.getEntry("tv").getDouble(0) == 1;
    }
}
