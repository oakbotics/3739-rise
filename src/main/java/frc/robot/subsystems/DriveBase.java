/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.Faults;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.TalonFXConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.controller.SimpleMotorFeedforward;
import edu.wpi.first.wpilibj.geometry.Pose2d;
import edu.wpi.first.wpilibj.geometry.Rotation2d;
import edu.wpi.first.wpilibj.kinematics.ChassisSpeeds;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.wpilibj.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Units;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveBase extends SubsystemBase {

  private final static double kEncoderCPRtoInches = 6 * Math.PI / (2048 * 10.9);
  private final static double kS = 0.144, kV = 0.0591, kA = 0.00805, kP = 1.05, kD = 0;

  private SimpleMotorFeedforward m_ff;

  private AHRS m_ahrs;
  private DifferentialDriveOdometry m_odometry;
  private DifferentialDriveKinematics m_kinematics;
  private ChassisSpeeds m_chassisSpeeds;
  private DifferentialDriveWheelSpeeds m_wheelSpeeds;

  private WPI_TalonFX mtrLTop, mtrLBottom, mtrRTop, mtrRBottom;

  private PIDController m_leftPidController;
  private PIDController m_rightPidController;

  /**
   * Creates a new DriveBase.
   */
  public DriveBase() {

    m_ff = new SimpleMotorFeedforward(kS, kV, kA);

    mtrLTop = new WPI_TalonFX(Constants.CAN_ID_MTR_LEFT_TOP);
    mtrLBottom = new WPI_TalonFX(Constants.CAN_ID_MTR_LEFT_BOTTOM);
    mtrRTop = new WPI_TalonFX(Constants.CAN_ID_MTR_RIGHT_TOP);
    mtrRBottom = new WPI_TalonFX(Constants.CAN_ID_MTR_RIGHT_BOTTOM);

    TalonFXConfiguration fxConfig = new TalonFXConfiguration();
    fxConfig.openloopRamp = .25;
    fxConfig.closedloopRamp = .25;
    fxConfig.primaryPID.selectedFeedbackSensor = FeedbackDevice.IntegratedSensor;
    fxConfig.supplyCurrLimit = new SupplyCurrentLimitConfiguration(true, 40, 80, 250);

    mtrLTop.setInverted(InvertType.InvertMotorOutput);
    mtrLBottom.setInverted(InvertType.FollowMaster);
    mtrLBottom.follow(mtrLTop);

    mtrRTop.setInverted(InvertType.None);
    mtrRBottom.setInverted(InvertType.FollowMaster);
    mtrRBottom.follow(mtrRTop);

    mtrLTop.configAllSettings(fxConfig);
    mtrRTop.configAllSettings(fxConfig);
    mtrLBottom.configAllSettings(fxConfig);
    mtrRBottom.configAllSettings(fxConfig);

    mtrLTop.configVoltageCompSaturation(12);
    mtrRTop.configVoltageCompSaturation(12);
    mtrLBottom.configVoltageCompSaturation(12);
    mtrRBottom.configVoltageCompSaturation(12);

    m_leftPidController = new PIDController(kP, 0, kD);
    m_rightPidController = new PIDController(kP, 0, kD);

    m_ahrs = new AHRS(SPI.Port.kMXP);

    m_odometry = new DifferentialDriveOdometry(getGyroAngle());
    m_kinematics = new DifferentialDriveKinematics(Units.inchesToMeters(28));

    m_wheelSpeeds = new DifferentialDriveWheelSpeeds(0, 0);
    m_chassisSpeeds = m_kinematics.toChassisSpeeds(m_wheelSpeeds);

    setBrakeMode(false);
    resetEncoders();
    resetGyro();
  }

  public SimpleMotorFeedforward getDrivetrainFeedforward() {
    return m_ff;
  }

  public PIDController getLeftPidController() {
    return m_leftPidController;

  }

  public PIDController getRightPidController() {
    return m_rightPidController;
  }

  public DifferentialDriveKinematics getKinematics() {
    return m_kinematics;
  }

  public Rotation2d getGyroAngle() {
    return Rotation2d.fromDegrees(-m_ahrs.getAngle());
  }

  public Pose2d getRobotPose() {
    return m_odometry.getPoseMeters();
  }

  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return m_wheelSpeeds;
  }

  public ChassisSpeeds getChassisSpeeds() {
    return m_chassisSpeeds;
  }

  public void setBrakeMode(boolean inBrakeMode) {
    if (inBrakeMode) {
      mtrLTop.setNeutralMode(NeutralMode.Brake);
      mtrLBottom.setNeutralMode(NeutralMode.Brake);
      mtrRTop.setNeutralMode(NeutralMode.Brake);
      mtrRBottom.setNeutralMode(NeutralMode.Brake);
    }
    else {
      mtrLTop.setNeutralMode(NeutralMode.Coast);
      mtrLBottom.setNeutralMode(NeutralMode.Coast);
      mtrRTop.setNeutralMode(NeutralMode.Coast);
      mtrRBottom.setNeutralMode(NeutralMode.Coast);
    }
  }

  public void setVoltageCompensation(boolean isCompensating) {
    mtrLTop.enableVoltageCompensation(isCompensating);
    mtrLBottom.enableVoltageCompensation(isCompensating);
    mtrRTop.enableVoltageCompensation(isCompensating);
    mtrRBottom.enableVoltageCompensation(isCompensating);
  }

  public void arcadeDrive(double speed, double turn) {
    mtrLTop.set(ControlMode.PercentOutput, speed + turn);
    mtrRTop.set(ControlMode.PercentOutput, speed - turn);
  }

  public void setOutputVolts(double leftVolts, double rightVolts) {
    mtrLTop.set(ControlMode.PercentOutput, leftVolts/12.0);
    mtrRTop.set(ControlMode.PercentOutput, rightVolts/12.0);
  }

  public double getLDistance() {
    return -kEncoderCPRtoInches * (mtrLTop.getSelectedSensorPosition() + mtrLBottom.getSelectedSensorPosition()) / 2;
  }

  public double getRDistance() {
    return kEncoderCPRtoInches * (mtrRTop.getSelectedSensorPosition() + mtrRBottom.getSelectedSensorPosition()) / 2;
  }

  public double getLSpeed() {
    return -kEncoderCPRtoInches * (mtrRTop.getSelectedSensorVelocity() + mtrLBottom.getSelectedSensorVelocity()) / 2;
  }

  public double getRSpeed() {
    return kEncoderCPRtoInches * (mtrRTop.getSelectedSensorVelocity() + mtrRBottom.getSelectedSensorVelocity()) / 2;
  }

  public void resetEncoders() {
    mtrLBottom.setSelectedSensorPosition(0);
    mtrRBottom.setSelectedSensorPosition(0);
    mtrLTop.setSelectedSensorPosition(0);
    mtrRTop.setSelectedSensorPosition(0);
  }

  public void resetAll() {
    resetEncoders();
    resetGyro();
    m_odometry.resetPosition(new Pose2d(0, 0, new Rotation2d()), getGyroAngle());
  }

  public void resetGyro() {
    m_ahrs.reset();
  }

  public void calibrateGyro() {
    m_ahrs.calibrate();
  }

  @Override
  public void periodic() {
    m_odometry.update(getGyroAngle(), Units.inchesToMeters(getLDistance()), Units.inchesToMeters(getRDistance()));
    m_wheelSpeeds = new DifferentialDriveWheelSpeeds(Units.inchesToMeters(getLSpeed()), Units.inchesToMeters(getRSpeed()));
    m_chassisSpeeds = m_kinematics.toChassisSpeeds(m_wheelSpeeds);

    Faults leftAFaults = new Faults();
    Faults leftBFaults = new Faults();
    Faults rightAFaults = new Faults();
    Faults rightBFaults = new Faults();

    mtrLTop.getFaults(leftAFaults);
    mtrLBottom.getFaults(leftBFaults);
    mtrRTop.getFaults(rightAFaults);
    mtrRBottom.getFaults(rightBFaults);

    if (leftAFaults.HardwareFailure || leftAFaults.ResetDuringEn || leftAFaults.SensorOverflow || leftAFaults.UnderVoltage || leftAFaults.SupplyOverV || leftAFaults.SupplyUnstable) {
      SmartDashboard.putBoolean("Drive Train Fault", true);
      DriverStation.reportError("Drive Train - Left Top Talon FX Failure " + leftAFaults.toString(), false);
      mtrLTop.clearStickyFaults();
    }
    if (leftBFaults.HardwareFailure || leftBFaults.ResetDuringEn || leftBFaults.SensorOverflow || leftBFaults.UnderVoltage || leftBFaults.SupplyOverV || leftBFaults.SupplyUnstable) {
      SmartDashboard.putBoolean("Drive Train Fault", true);
      DriverStation.reportError("Drive Train - Left Bottom Talon FX Failure " + leftBFaults.toString(), false);
      mtrLBottom.clearStickyFaults();
    }
    if (rightAFaults.HardwareFailure || rightAFaults.ResetDuringEn || rightAFaults.SensorOverflow || rightAFaults.UnderVoltage || rightAFaults.SupplyOverV || rightAFaults.SupplyUnstable) {
      SmartDashboard.putBoolean("Drive Train Fault", true);
      DriverStation.reportError("Drive Train - Right Top Talon FX Failure " + rightAFaults.toString(), false);
      mtrRTop.clearStickyFaults();
    }
    if (rightBFaults.HardwareFailure || rightBFaults.ResetDuringEn || rightBFaults.SensorOverflow || rightBFaults.UnderVoltage || rightBFaults.SupplyOverV || rightBFaults.SupplyUnstable) {
      SmartDashboard.putBoolean("Drive Train Fault", true);
      DriverStation.reportError("Drive Train - Right Bottom Talon FX Failure " + rightBFaults.toString(), false);
      mtrRBottom.clearStickyFaults();
    }
  }
}
