package frc.robot.subsystems;

import com.revrobotics.ColorSensorV3;

import edu.wpi.first.wpilibj.I2C.Port;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class ColorSystem extends SubsystemBase {

    public static class HSLColor {
        private double m_hue, m_saturation, m_lightness;
        
        public HSLColor(double hue, double saturation, double lightness) {
            m_hue = hue;
            m_saturation = saturation;
            m_lightness = lightness;
        }

        public double getHue() {
            return m_hue;
        }
        public double getSaturation() {
            return m_saturation;
        }
        public double getLightness() {
            return m_lightness;
        }
    }
    
    private final ColorSensorV3 m_colorSensor;

    public ColorSystem() {
        m_colorSensor = new ColorSensorV3(Port.kOnboard);
    }

    public HSLColor getSensorHSL() {
        return rgbtohsl(m_colorSensor.getRed(), m_colorSensor.getGreen(), m_colorSensor.getBlue());
    }

    public ColorSensorV3 getSensor() {
        return m_colorSensor;
    }

    public static HSLColor rgbtohsl(final double r, final double g, final double b) {
        final long regMax = 1048576;
        final double r2 = r / regMax, g2 = g / regMax, b2 = b / regMax;
        Math.max(Math.max(r2, g2),b2);
        
        return new HSLColor(0,0,0); 
    }
}