package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.Faults;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class ConveyorSystem extends SubsystemBase {

    public static enum ConveyorLocation {
        kTop,
        kFront,
        kBack
    };

    private VictorSPX m_conveyorMotor;

    public ConveyorSystem(ConveyorLocation location) {
        if (location == ConveyorLocation.kFront) {
            m_conveyorMotor = new VictorSPX(Constants.CAN_ID_MTR_CONVEYOR_ROLLER_FRONT);
            m_conveyorMotor.setInverted(InvertType.InvertMotorOutput);
            setName("Front Conveyor");
        }
        else if (location == ConveyorLocation.kBack) {
            m_conveyorMotor = new VictorSPX(Constants.CAN_ID_MTR_CONVEYOR_ROLLER_BACK);
            m_conveyorMotor.setInverted(InvertType.InvertMotorOutput);
            setName("Rear Conveyor");
        }
        else {
            m_conveyorMotor = new VictorSPX(Constants.CAN_ID_MTR_CONVEYOR_ROLLER_TOP);
            m_conveyorMotor.setInverted(InvertType.None);
            setName("Top Conveyor");
        }
    }

    @Override
    public void periodic() {
        Faults faults = new Faults();

        m_conveyorMotor.getFaults(faults);

        if (faults.HardwareFailure || faults.ResetDuringEn || faults.UnderVoltage) {
            SmartDashboard.putBoolean(getName() + " Fault", true);
            DriverStation.reportError(getName() + " - Victor SPX Failure" + faults.toString(), false);
            m_conveyorMotor.clearStickyFaults();
        }
    }

    public void setIntakeSpeed(double speed) {
        m_conveyorMotor.set(ControlMode.PercentOutput, speed);
    }

    public void stop() {
        m_conveyorMotor.set(ControlMode.PercentOutput, 0);
    }
}