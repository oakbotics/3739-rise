package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.Faults;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.ctre.phoenix.motorcontrol.can.VictorSPXConfiguration;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class WheelSpinnerKicker extends SubsystemBase {
    private VictorSPX m_stopperWheelMtr;
    
    public WheelSpinnerKicker() {
        m_stopperWheelMtr = new VictorSPX(Constants.CAN_ID_MTR_CONVEYOR_STOPPER_WHEEL);
        m_stopperWheelMtr.configFactoryDefault();
      
        VictorSPXConfiguration stopperWheelMtrConfig = new VictorSPXConfiguration();
        stopperWheelMtrConfig.openloopRamp = 0.15;

        m_stopperWheelMtr.configAllSettings(stopperWheelMtrConfig);
        m_stopperWheelMtr.setInverted(InvertType.InvertMotorOutput); 
        m_stopperWheelMtr.setNeutralMode(NeutralMode.Brake);
    }

    @Override
    public void periodic() {
        Faults faults = new Faults();

        m_stopperWheelMtr.getFaults(faults);

        if (faults.HardwareFailure || faults.ResetDuringEn || faults.UnderVoltage) {
            DriverStation.reportError("Color Wheel / Kicker - Victor SPX Failure " + faults.toString(), false);
            SmartDashboard.putBoolean("Kicker Fault", true);
            m_stopperWheelMtr.clearStickyFaults();
        }
    }

    public void runKicker() {
        m_stopperWheelMtr.set(ControlMode.PercentOutput, 1);
    }

    public void reverseKicker() {
        m_stopperWheelMtr.set(ControlMode.PercentOutput, -1);
    }

    public void stop() {
        m_stopperWheelMtr.set(ControlMode.PercentOutput, 0);
    }
}