package frc.robot.commands;

import java.util.function.DoubleSupplier;

import frc.robot.subsystems.ConveyorSystem;
import edu.wpi.first.wpilibj2.command.CommandBase;


public class TeleOpConveyor extends CommandBase {
    private DoubleSupplier m_input;
    private ConveyorSystem m_conveyor;

    public TeleOpConveyor(DoubleSupplier input, ConveyorSystem conveyor) {
        m_input = input;
        m_conveyor = conveyor; 

        addRequirements(conveyor);
    }

    @Override
    public void execute() {
        m_conveyor.setIntakeSpeed(m_input.getAsDouble());
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_conveyor.stop();
    }
}