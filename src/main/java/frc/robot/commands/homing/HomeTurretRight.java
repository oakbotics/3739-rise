package frc.robot.commands.homing;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;

public class HomeTurretRight extends CommandBase {
    private Turret m_turret;
    private DigitalInput m_rightLimit;

    private boolean hitRightLimit;

    public HomeTurretRight(Turret turret) {
        m_turret = turret;
        m_rightLimit = turret.getRightLimit();

        addRequirements(turret);
    }

    @Override
    public void initialize() {
        hitRightLimit = false;
    }

    @Override
    public void execute() {
        if (!m_rightLimit.get()) {
            hitRightLimit = true;
            return;
        }
        m_turret.setPower(-0.3);
    }

    @Override
    public boolean isFinished() {
        return hitRightLimit;
    }
    
    @Override
    public void end(boolean interrupted) {
        m_turret.stop();
    }
}