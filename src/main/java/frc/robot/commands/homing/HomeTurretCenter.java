package frc.robot.commands.homing;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;

public class HomeTurretCenter extends CommandBase {
    private Turret m_turret;

    private double m_homingAngle;

    public HomeTurretCenter(Turret turret) {
        m_turret = turret;

        m_homingAngle = turret.getCurrentAngle() - 90;

        addRequirements(turret);
    }

    @Override
    public void execute() {
        m_turret.setAbsoluteSetpoint(m_homingAngle);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        if (!interrupted) {
            m_turret.resetEncoder();
        }
        m_turret.stop();
    }
}