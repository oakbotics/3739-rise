package frc.robot.commands.homing;

import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;

public class HomeTurretLeft extends CommandBase {
    private Turret m_turret;
    private DigitalInput m_leftLimit;
    private DigitalInput m_rightLimit;

    private boolean hitRightLimit;
    private boolean hitLeftLimit;

    public HomeTurretLeft(Turret turret) {
        m_turret = turret;
        m_leftLimit = turret.getLeftLimit();
        m_rightLimit = turret.getRightLimit();

        addRequirements(turret);
    }

    @Override
    public void initialize() {
        m_turret.resetEncoder();
        hitRightLimit = false;
        hitLeftLimit = false;
    }

    @Override
    public void execute() {
        if (!m_leftLimit.get()) {
            hitLeftLimit = true;
            return;
        }
        else if (!m_rightLimit.get()) {
            hitRightLimit = true;
            return;
        }
        m_turret.setPower(0.3);
    }

    @Override
    public boolean isFinished() {
        return hitRightLimit || hitLeftLimit;
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.stop();
        if (!interrupted) {
            m_turret.resetEncoder();
        }
    }
}