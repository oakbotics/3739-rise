package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class ManualFlywheel extends CommandBase {
    private Shooter m_Shooter;
    public ManualFlywheel(Shooter shooter) {
        m_Shooter = shooter;
        addRequirements(shooter);
        SmartDashboard.putNumber("Top RPM", 0);
        SmartDashboard.putNumber("Bottom RPM", 0);

    }

    @Override
    public void execute() {
        double topSpeed = SmartDashboard.getNumber("Top RPM", 0);
        double bottomSpeed = SmartDashboard.getNumber("Bottom RPM", 0);
        m_Shooter.setFlywheelSpeeds(topSpeed, bottomSpeed);
    
    }

    @Override
    public void end(boolean interrupted) {
        m_Shooter.stop();
    }

}