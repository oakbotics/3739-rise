package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;

public class HoldTurretPosition extends CommandBase {
    private final static double kStabilizeTimeS = 0.05;
    private Timer m_timer;
    private boolean m_timerStarted;

    private Turret m_turret;
    
    public HoldTurretPosition(Turret turret) {
        m_turret = turret;
        m_timer = new Timer();

        addRequirements(m_turret);
    }
    
    @Override
    public void initialize() {
        m_timer.reset();
    }

    @Override
    public void execute() {
        m_turret.holdSetpoint();
    }

    @Override
    public boolean isFinished() {
        if (m_turret.pidOnTarget()) {
            if (m_timerStarted) {
                return m_timer.hasElapsed(kStabilizeTimeS);
            }
            else {
                m_timer.start();
                m_timerStarted = true;
            }
        }
        else {
            m_timerStarted = false;
            m_timer.stop();
            m_timer.reset();
        }
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.stop();
        m_timer.stop();
        m_timer.reset();
    }
}