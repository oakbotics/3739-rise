package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.VisionSystem;

public class SpinFlywheel extends CommandBase {
    public static enum FlywheelState {
        kSpinningUp,
        kStabilizing,
        kAtSpeed
    }

    private Shooter m_shooter;
    private VisionSystem m_vision;

    private final double kRPMThreshold = 25, kTimeThresholdMS = 50;
    private Timer m_rpmTimer;

    private FlywheelState m_currentState;

    public SpinFlywheel(Shooter shooter, VisionSystem vision) {
        m_shooter = shooter;
        m_vision = vision;
        m_rpmTimer = new Timer();

        addRequirements(m_shooter);
    }

    @Override
    public void initialize() {
        m_currentState = FlywheelState.kSpinningUp;
        SmartDashboard.putBoolean("Flywheel Locked", false);
        m_vision.setTargeting(true);
    }

    @Override
    public void execute() {
        if (m_vision.hasTarget()) {
            m_shooter.shootToDistance(m_vision.getLemonLaunchDistance());
            // State machine
            switch (m_currentState) {
                case kSpinningUp:
                    if (m_shooter.rpmOnTarget(kRPMThreshold)) {
                        // transition to Stabilizing state when rpm is at the threshold
                        m_currentState = FlywheelState.kStabilizing;
                        m_rpmTimer.start();
                        SmartDashboard.putBoolean("Flywheel Locked", false);
                        System.out.println("I like it");
                    }
                    break;
                case kStabilizing: 
                    if (!m_shooter.rpmOnTarget(kRPMThreshold)) {
                        // transition to SpinningUp state if rpm is beyond the threshold
                        m_currentState = FlywheelState.kSpinningUp;
                        m_rpmTimer.stop();
                    }
                    else if (m_rpmTimer.hasElapsed(kTimeThresholdMS)) {
                        // transition to AtSpeed state when the on target timer has lapsed
                        m_currentState = FlywheelState.kAtSpeed;
                        m_rpmTimer.stop();
                        SmartDashboard.putBoolean("Flywheel Locked", true);
                        System.out.println("I want it");
                    }
                    break;
                case kAtSpeed: 
                    if (!m_shooter.rpmOnTarget(kRPMThreshold)) {
                        // transition to SpinningUp state if rpm is beyond the threshold
                        m_currentState = FlywheelState.kSpinningUp;
                        SmartDashboard.putBoolean("Flywheel Locked", false);
                    }
                    break;
            }
        }
    }

    @Override
    public void end(boolean interrupted) {
        m_shooter.stop();
        SmartDashboard.putBoolean("Flywheel Locked", false);
        m_vision.setTargeting(false);
    }
}