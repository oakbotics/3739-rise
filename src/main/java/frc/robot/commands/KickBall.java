package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.ConveyorSystem;
import frc.robot.subsystems.VisionSystem;
import frc.robot.subsystems.WheelSpinnerKicker;

public class KickBall extends CommandBase {
    private static enum KickerState {
        kRunningKicker(0.32),
        kWait(0.06),
        kRunningConveyor(0.2);
        private final double m_runPeriod;

        private KickerState(double forPeriodS) {
            m_runPeriod = forPeriodS;
        }

        private double getRunPeriod() {
            return m_runPeriod;
        }
    }

    private WheelSpinnerKicker m_kicker;
    private ConveyorSystem m_topConveyor;
    private VisionSystem m_vision;

    private Timer m_timer; 

    private KickerState m_state;

    public KickBall(WheelSpinnerKicker kicker, ConveyorSystem topConveyor, VisionSystem vision) {
        m_kicker = kicker;
        m_topConveyor = topConveyor;
        m_vision = vision;
        m_timer = new Timer();

        addRequirements(m_kicker);
    }
    
    @Override
    public void initialize() {
        m_state = KickerState.kRunningKicker;
        if (m_vision.hasTarget()) System.out.println("I got it");
        m_timer.reset();
        m_timer.start();
    }

    @Override
    public void execute() {
        // state machine run kicker then wait a small amount and draw the conveyor up
        switch(m_state) {
            case kRunningKicker:
                System.out.println("kick");
                m_kicker.runKicker();
                if (m_timer.advanceIfElapsed(m_state.getRunPeriod())) m_state = KickerState.kWait;
                break;
            case kWait:
            System.out.println("wait");
                if (m_timer.advanceIfElapsed(m_state.getRunPeriod())) m_state = KickerState.kRunningConveyor;
                break;
            case kRunningConveyor:
            System.out.println("convey");
                m_topConveyor.setIntakeSpeed(1.0);
                break;
        }
    }

    @Override
    public boolean isFinished() {
        if (m_state == KickerState.kRunningConveyor) {
            return m_timer.advanceIfElapsed(m_state.getRunPeriod());
        }
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_kicker.stop();
        m_timer.stop();
    }
}