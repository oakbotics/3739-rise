package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;

public class AdjustTurret extends CommandBase {

    private Turret m_turret;
    private DoubleSupplier m_adjustSpeed;

    private Timer m_timer;

    // Degrees per second
    private final double kDegPerS = 60;

    public AdjustTurret(Turret turret, DoubleSupplier adjustSpeed) {
        m_turret = turret;
        m_adjustSpeed = adjustSpeed;
        m_timer = new Timer();

        addRequirements(m_turret);
    }

    @Override
    public void initialize() {
        m_timer.reset();
        m_timer.start();
    }

    @Override
    public void execute() {
        // dt * (dTheta / dt) * scale

        double dt = m_timer.get();
        double angle = dt * kDegPerS * m_adjustSpeed.getAsDouble();
        SmartDashboard.putNumber("Turret Requested Angle", angle);
        double curset = m_turret.getCurrentSetpoint();
        m_turret.setAbsoluteSetpoint(curset + angle);
        m_turret.holdSetpoint();

        m_timer.reset();
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.stop();
        m_timer.stop();
        m_timer.reset();
    }

}