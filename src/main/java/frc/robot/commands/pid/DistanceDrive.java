package frc.robot.commands.pid;

import edu.wpi.first.wpilibj.controller.PIDController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpiutil.math.MathUtil;
import frc.robot.subsystems.DriveBase;

public class DistanceDrive extends CommandBase {

    private PIDController m_pidController;
    private DriveBase m_driveBase;

    public DistanceDrive(DriveBase driveBase, double setpoint) {
        m_driveBase = driveBase;
        m_pidController = new PIDController(0.015, 0, 0.01);
        m_pidController.setSetpoint(setpoint);
        addRequirements(m_driveBase);
    }

    @Override
    public void execute() {
        double output = MathUtil.clamp(-m_pidController.calculate((m_driveBase.getLDistance() + m_driveBase.getRDistance()) / 2), -0.25, 0.25);

        SmartDashboard.putNumber("PIDOutput", output);
        SmartDashboard.putNumber("PIDError", m_pidController.getPositionError());

        m_driveBase.arcadeDrive(output, 0);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        m_driveBase.arcadeDrive(0, 0);
    }
}