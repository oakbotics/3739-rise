package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;

public class SetTurretAngle extends CommandBase {

    private double m_absoluteAngle;
    private Turret m_turret;

    public SetTurretAngle(Turret turret, double absoluteAngle) {
        m_absoluteAngle = absoluteAngle;
        m_turret = turret;
        addRequirements(turret);
    }

    @Override
    public void execute() {
        m_turret.setAbsoluteSetpoint(m_absoluteAngle);
    }

    @Override
    public void end(boolean interrupted) {
        m_turret.stop();
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}