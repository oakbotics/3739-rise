package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Shooter;

public class TeleOpShooter extends CommandBase {
    private XboxController m_operatorController;
    private Shooter m_shooter; 

    public TeleOpShooter(Shooter shooter, XboxController operatorController) {
        m_operatorController = operatorController;
        m_shooter = shooter;

        addRequirements(m_shooter);

    }
    
    @Override
    public void execute() {
        m_shooter.setShooterPower(
        m_operatorController.getTriggerAxis(Hand.kRight),
        m_operatorController.getTriggerAxis(Hand.kLeft));
    }

}