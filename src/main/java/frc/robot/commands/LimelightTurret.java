package frc.robot.commands;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Turret;
import frc.robot.subsystems.VisionSystem;

public class LimelightTurret extends CommandBase {
    private Turret m_turret;
    private VisionSystem m_visionSystem;

    public LimelightTurret(Turret turret, VisionSystem visionSystem) {
        m_turret = turret;
        m_visionSystem = visionSystem;

        addRequirements(m_turret, m_visionSystem);
        
        SmartDashboard.putBoolean("Turret Locked", false);
    }

    @Override
    public void initialize() {
        m_visionSystem.setTargeting(true);
        System.out.println("I see it");
        m_turret.stop();
    }

    @Override
    public void execute() {
        if (m_visionSystem.hasTarget()) {
            double setPoint = -m_visionSystem.getHorizontalAngle();
            SmartDashboard.putNumber("Target Diff", setPoint);
            double curAngle = m_turret.getCurrentAngle();
            m_turret.setAbsoluteSetpoint(curAngle - setPoint);
            m_turret.holdSetpoint();
            SmartDashboard.putBoolean("Turret Locked", m_turret.pidOnTarget());
        }
    }

    @Override
    public void end(boolean interrupted) {
        m_visionSystem.setTargeting(false);
        m_turret.stop();
        SmartDashboard.putBoolean("Turret Locked", false);
    }
}