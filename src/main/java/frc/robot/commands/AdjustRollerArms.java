package frc.robot.commands;

import java.util.function.DoubleSupplier;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.RollerArms;

public class AdjustRollerArms extends CommandBase {

    private RollerArms m_rollerArms;
    private DoubleSupplier m_adjustSpeed;

    private Timer m_timer;

    // Degrees per second
    private final double kDegPerS = 90;

    public AdjustRollerArms(RollerArms rollerArms, DoubleSupplier adjustSpeed) {
        m_rollerArms = rollerArms;
        m_adjustSpeed = adjustSpeed;

        m_timer = new Timer();

        addRequirements(m_rollerArms);
    }

    @Override
    public void initialize() {
        m_timer.reset();
        m_timer.start();
    }

    @Override
    public void execute() {
        // dt * (dTheta / dt) * scale
        double angle = m_timer.get() * kDegPerS * m_adjustSpeed.getAsDouble();
        SmartDashboard.putNumber("Arms Requested Angle", angle);
        m_rollerArms.setRelativeSetpoint(angle);

        m_timer.reset();
    }

    @Override
    public void end(boolean interrupted) {
        m_rollerArms.stop();
        m_timer.stop();
        m_timer.reset();
    }
}