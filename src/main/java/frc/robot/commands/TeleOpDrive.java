
/*----------------------------------------------------------------------------*/
/* Copyright (c) 2019 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj2.command.CommandBase;

import frc.robot.subsystems.DriveBase;

public class TeleOpDrive extends CommandBase {
  private final DriveBase m_DriveBase;
  private final XboxController m_DriveController;
  /**
   * Creates a new TeleOpDrive.
   */
  public TeleOpDrive(final DriveBase driveBase, final XboxController controller) {
    m_DriveBase = driveBase;
    m_DriveController = controller;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(driveBase);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    double speed = -m_DriveController.getY(Hand.kLeft);
    speed = Math.abs(speed) <= 0.05 ? 0 : Math.pow(speed, 2) * Math.signum(speed);
    double turn = m_DriveController.getX(Hand.kRight);
    turn = Math.abs(turn) <= 0.05 ? 0 : Math.pow(turn, 2) * Math.signum(turn);
 
    if (m_DriveController.getTriggerAxis(Hand.kRight) >= 0.2) {
      m_DriveBase.arcadeDrive(speed, 0.75 * turn);
    }
    else if (m_DriveController.getTriggerAxis(Hand.kLeft) >= 0.2) {
      m_DriveBase.arcadeDrive(0.25 * speed, 0.25 * turn);
    }
    else {
      m_DriveBase.arcadeDrive(0.75 * speed, 0.75 * turn);
    }
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(final boolean interrupted) {
  }

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
