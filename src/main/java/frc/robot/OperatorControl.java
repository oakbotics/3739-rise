package frc.robot;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.button.Trigger;

public class OperatorControl {
    private static enum ControlDirection {
        kFront,
        kBack
    }

    private XboxController m_controller;
    private ControlDirection m_currentDirection;

    public OperatorControl(XboxController controller) {
        m_controller = controller;
        m_currentDirection = ControlDirection.kFront;
        SmartDashboard.putBoolean("Control Direction", true);
    }

    public void toggleDirection() {
        if (m_currentDirection == ControlDirection.kFront) {
            m_currentDirection = ControlDirection.kBack;
            SmartDashboard.putBoolean("Control Direction", false);
        }
        else {
            m_currentDirection = ControlDirection.kFront;
            SmartDashboard.putBoolean("Control Direction", true);
        }
    }

    public XboxController getController() {
        return m_controller;
    }

    public double getFrontIntakeSpeed() {
        return m_controller.getTriggerAxis(Hand.kRight) - m_controller.getTriggerAxis(Hand.kLeft);

    }
    
    public double getBackIntakeSpeed() {
        return m_controller.getTriggerAxis(Hand.kRight) - m_controller.getTriggerAxis(Hand.kLeft);

    }

    public double getTopIntakeSpeed() {
        return -m_controller.getTriggerAxis(Hand.kRight);
       // return m_controller.getTriggerAxis(Hand.kRight) - m_controller.getTriggerAxis(Hand.kLeft);
    }

    public double getTurretAdjustment() {
        double val = m_controller.getX(Hand.kRight);
        return Math.abs(val) > 0.15 ? val : 0;
    }

    public double getFrontArmAdjustment() {
        if (m_currentDirection == ControlDirection.kFront) {
            double val = m_controller.getY(Hand.kLeft);
            return Math.abs(val) > 0.15 ? val : 0;
        }
        return 0;
    }

    public double getBackArmAdjustment() {
        if (m_currentDirection == ControlDirection.kBack) {
            double val = m_controller.getY(Hand.kLeft);
            return Math.abs(val) > 0.15 ? val : 0;
        }
        return 0;
    }

    public Trigger getFrontArmTrigger() {
        return new Trigger(() -> m_currentDirection == ControlDirection.kFront && m_controller.getBumperPressed(Hand.kLeft));
    }

    public Trigger getBackArmTrigger() {
        return new Trigger(() -> m_currentDirection == ControlDirection.kBack && m_controller.getBumperPressed(Hand.kLeft));
    }

    public Trigger getToggleDirectionTrigger() {
        return new Trigger(() -> m_controller.getPOV() == 0 || m_controller.getPOV() == 180);
    }
} 