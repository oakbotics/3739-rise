/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants.  This class should not be used for any other purpose.  All constants should be
 * declared globally (i.e. public static).  Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    // Drive Motors
    public static final int CAN_ID_MTR_LEFT_TOP = 5;
    public static final int CAN_ID_MTR_LEFT_BOTTOM = 4;
    public static final int CAN_ID_MTR_RIGHT_TOP = 3;
    public static final int CAN_ID_MTR_RIGHT_BOTTOM = 2;

    // Turret Motor
    public static final int CAN_ID_MTR_TURRET = 6;

    // Shooter Motors
    public static final int CAN_ID_MTR_SHOOTER_TOPA = 7;
    public static final int CAN_ID_MTR_SHOOTER_BOTTOMA = 8;
    public static final int CAN_ID_MTR_SHOOTER_BOTTOMB = 9;

    // Conveyor / Roller Motors
    public static final int CAN_ID_MTR_CONVEYOR_STOPPER_WHEEL = 10;
    public static final int CAN_ID_MTR_CONVEYOR_ROLLER_FRONT = 11;
    public static final int CAN_ID_MTR_CONVEYOR_ROLLER_BACK = 12;
    public static final int CAN_ID_MTR_CONVEYOR_ROLLER_TOP = 13;
      
    // Roller-Arm Motors
    public static final int CAN_ID_MTR_ROLLER_ARMS_FRONT = 14;
    public static final int CAN_ID_MTR_ROLLER_ARMS_BACK = 15;

    // Climber Motors
    public static final int CAN_ID_MTR_CLIMBER_LEFT = 16;
    public static final int CAN_ID_MTR_CLIMBER_RIGHT = 17;
}
