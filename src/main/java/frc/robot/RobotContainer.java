/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018-2019 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package frc.robot;

import java.nio.file.Path;

import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.controller.RamseteController;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.trajectory.Trajectory;
import edu.wpi.first.wpilibj.trajectory.TrajectoryUtil;
import frc.robot.commands.AdjustRollerArms;
import frc.robot.commands.AdjustTurret;
import frc.robot.commands.HoldTurretPosition;
import frc.robot.commands.KickBall;
import frc.robot.commands.LimelightTurret;
import frc.robot.commands.ManualFlywheel;
import frc.robot.commands.SpinFlywheel;
import frc.robot.commands.TeleOpConveyor;
import frc.robot.commands.TeleOpDrive;
import frc.robot.commands.homing.HomeTurretCenter;
import frc.robot.commands.homing.HomeTurretLeft;
import frc.robot.commands.homing.HomeTurretRight;
import frc.robot.subsystems.ConveyorSystem;
import frc.robot.subsystems.DriveBase;
import frc.robot.subsystems.RollerArms;
import frc.robot.subsystems.Shooter;
import frc.robot.subsystems.Turret;
import frc.robot.subsystems.VisionSystem;
import frc.robot.subsystems.WheelSpinnerKicker;
import frc.robot.subsystems.ConveyorSystem.ConveyorLocation;
import frc.robot.subsystems.RollerArms.RollerArmsLocation;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.InstantCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import edu.wpi.first.wpilibj2.command.RunCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;

/**
 * This class is where the bulk of the robot should be declared.  Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls).  Instead, the structure of the robot
 * (including subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...

  public final DriveBase m_driveBase = new DriveBase();  
  public final Shooter m_shooter = new Shooter();
  public final Turret m_turret = new Turret(); 
  public final ConveyorSystem m_frontConveyor = new ConveyorSystem(ConveyorLocation.kFront);
  //public final ConveyorSystem m_backConveyor = new ConveyorSystem(ConveyorLocation.kBack);
  public final ConveyorSystem m_topConveyor = new ConveyorSystem(ConveyorLocation.kTop);
  public final WheelSpinnerKicker m_kicker = new WheelSpinnerKicker();
  public final RollerArms m_frontArms = new RollerArms(RollerArmsLocation.kFront);
  // public final RollerArms m_backArms = new RollerArms(RollerArmsLocation.kBack);

  public final XboxController m_driveController = new XboxController(0);
  public final OperatorControl m_operatorControl = new OperatorControl(new XboxController(1));
  
  public final VisionSystem m_visionSystem = new VisionSystem();

  public Trajectory m_autoTrajectory;

  private SendableChooser<Integer> m_chooser;
  
  /**
   * The container for the robot.  Contains subsystems, OI devices, and commands.
   */
  public RobotContainer() {
    m_frontConveyor.setDefaultCommand(new TeleOpConveyor(m_operatorControl::getFrontIntakeSpeed, m_frontConveyor));
    //m_backConveyor.setDefaultCommand(new TeleOpConveyor(m_operatorControl::getBackIntakeSpeed, m_backConveyor));
    m_topConveyor.setDefaultCommand(new TeleOpConveyor(m_operatorControl::getTopIntakeSpeed, m_topConveyor));
    m_driveBase.setDefaultCommand(new TeleOpDrive(m_driveBase, m_driveController));
    m_frontArms.setDefaultCommand(new RunCommand(() -> m_frontArms.holdPosition(), m_frontArms));

    m_chooser = new SendableChooser<>();
    m_chooser.addOption("sCurve", 0);
    m_chooser.addOption("forward", 1);
    m_chooser.setDefaultOption("forward", 1);
    SmartDashboard.putData("Auto Chooser", m_chooser);

    SmartDashboard.putData("Reset Gyro", new InstantCommand(m_driveBase::resetGyro));

    SequentialCommandGroup homeTurretProcedure = new SequentialCommandGroup(
      new HomeTurretLeft(m_turret),
      new HomeTurretRight(m_turret),
      new HomeTurretCenter(m_turret)
    );

    homeTurretProcedure.setName("Turret Homing");

    SmartDashboard.putData(homeTurretProcedure);

    var resetTurret = new InstantCommand(() -> m_turret.resetEncoder());
    resetTurret.setName("Zero Turret");

    var resetFrontRollers = new InstantCommand(() -> m_frontArms.zeroRollerSensor());
    resetFrontRollers.setName("Zero Front Arms");

    // var resetBackRollers = new InstantCommand(() -> m_backArms.zeroRollerSensor());
    // resetBackRollers.setName("Zero Back Arms");

    SmartDashboard.putData(resetTurret);
    SmartDashboard.putData(resetFrontRollers);
    // SmartDashboard.putData(resetBackRollers);

    var failsafeTurret = new RunCommand(() -> m_turret.setPower(m_operatorControl.getTurretAdjustment()), m_turret);
    failsafeTurret.setName("Manual Turret");
    
    var failsafeFrontRollers = new RunCommand(() -> m_frontArms.setPower(m_operatorControl.getFrontArmAdjustment()), m_frontArms);
    failsafeFrontRollers.setName("Manual Front Arms");

    // var failsafeBackRollers = new RunCommand(() -> m_backArms.setPower(m_operatorControl.getBackArmAdjustment()), m_backArms);
    // failsafeBackRollers.setName("Manual Back Arms");

    SmartDashboard.putData(failsafeTurret);
    SmartDashboard.putData(failsafeFrontRollers);
    // SmartDashboard.putData(failsafeBackRollers);

    // m_visionSystem.toggleCameraMode(false);
    m_visionSystem.setTargeting(false);

    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings.  Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a
   * {@link edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    // run the conveyor kicker procedure
    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kBumperRight.value)
      .whenPressed(new RunCommand(() -> m_kicker.runKicker(), m_kicker))
      .whenReleased(new InstantCommand(() -> m_kicker.stop(), m_kicker));
    
    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kY.value)
      .whileHeld(new RunCommand(() -> m_kicker.reverseKicker(), m_kicker))
      .whenReleased(new InstantCommand(() -> m_kicker.stop(), m_kicker));
    // Switch the operator control scheme to the opposite side
    m_operatorControl.getToggleDirectionTrigger()
    .whileActiveOnce(new InstantCommand(() -> m_operatorControl.toggleDirection()));

    // Fine adjust the turret
    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kStickRight.value)
      .toggleWhenPressed(new AdjustTurret(m_turret, m_operatorControl::getTurretAdjustment));
  
    // Lock on with the turret and spin up the flywheel  
    // new JoystickButton(m_operatorControl.getController(), XboxController.Button.kA.value)
    //   .whileHeld(new SpinFlywheel(m_shooter, m_visionSystem)
    //     .alongWith(new LimelightTurret(m_turret, m_visionSystem)));

    // Spin Shooter with ratio 
    // new JoystickButton(m_operatorControl.getController(), XboxController.Button.kA.value)
    // .whileHeld(new ManualFlywheel(m_shooter));

    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kX.value)
    .whileHeld(new SpinFlywheel(m_shooter, m_visionSystem));
  
    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kA.value)
      .whileHeld(new LimelightTurret(m_turret, m_visionSystem));

    // Toggle the position of the front arms
    m_operatorControl.getFrontArmTrigger()
      .whileActiveOnce(new InstantCommand(() -> m_frontArms.toggleArmSetpoint(true), m_frontArms));
    
    // // Toggle the position of the back arms
    // m_operatorControl.getBackArmTrigger()
    //   .whileActiveOnce(new InstantCommand(() -> m_backArms.toggleArmSetpoint(true), m_backArms));

    // Bring up both roller arms
    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kBack.value)
      .whenPressed(new InstantCommand(() -> {
        m_frontArms.setArmSetIndex(0);
      }, m_frontArms));

    // // Fine adjust the selected arm
    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kStickLeft.value)
      .toggleWhenPressed(new AdjustRollerArms(m_frontArms, m_operatorControl::getFrontArmAdjustment));

    // Home the turret
    new JoystickButton(m_operatorControl.getController(), XboxController.Button.kStart.value)
      .whenPressed(new InstantCommand(() -> m_turret.setAbsoluteSetpoint(0), m_turret)
        .andThen(new HoldTurretPosition(m_turret)));
  }
 

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An ExampleCommand will run in autonomous
    // TrajectoryConfig config = new TrajectoryConfig(Units.feetToMeters(6), Units.feetToMeters(9));
    
    // config.setKinematics(m_driveBase.getKinematics());

    // Trajectory trajectory = TrajectoryGenerator.generateTrajectory(
    //   new Pose2d(0, 0, new Rotation2d(0)),
    //   List.of(
    //     new Translation2d(1, 1),
    //     new Translation2d(2, -2)
    //   ),
    //   new Pose2d(3, 0, new Rotation2d(0)),
    //   config
    // );
    // return null;
    // Trajectory trajectory;

    // String path = null;

    // if (m_chooser.getSelected() == 0) {
    //   path = "paths/ForwardSCurve.wpilib.json";
    // }
    // else {
    //   path = "paths/Forward.wpilib.json";
    // }

    // System.out.println(path);

    // try {
    //   Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(path);
    //   trajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
    // }
    // catch (Exception e) {
    //   DriverStation.reportError("Unable to open trajectory", e.getStackTrace());
    //   return null;
    // }

    // var transform = m_driveBase.getRobotPose().minus(trajectory.getInitialPose());
    // m_autoTrajectory = trajectory.transformBy(transform);

    var command = new RunCommand(() -> m_shooter.setShooterPower(0.22, 0.22), m_shooter).withTimeout(10.0)
    .alongWith(
      new SequentialCommandGroup(
        new WaitCommand(3.0),
        new ParallelCommandGroup(
          new RunCommand(() -> m_kicker.runKicker(), m_kicker).withTimeout(0.26)
          .andThen(new InstantCommand(() -> m_kicker.stop(), m_kicker)),
          new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1.0), m_topConveyor).withTimeout(0.26)
          .andThen(new InstantCommand(() -> m_topConveyor.stop(), m_topConveyor))
        ),
        new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1.0), m_topConveyor).withTimeout(0.54)
        .andThen(new InstantCommand(() -> m_topConveyor.stop(), m_topConveyor)),
        new WaitCommand(1.0),
        new ParallelCommandGroup(
          new RunCommand(() -> m_kicker.runKicker(), m_kicker).withTimeout(0.26)
          .andThen(new InstantCommand(() -> m_kicker.stop(), m_kicker)),
          new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1.0), m_topConveyor).withTimeout(0.26)
          .andThen(new InstantCommand(() -> m_topConveyor.stop(), m_topConveyor))
        ),
        new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1.0), m_topConveyor).withTimeout(0.54)
      )
    );

    //var command = new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1.0), m_topConveyor).withTimeout(0.8);
      
    return command;

    // var shootBall = new RunCommand(() -> m_shooter.setShooterPower(.22, .22), m_shooter).withTimeout(15.0)
    // .alongWith(
    //   new SequentialCommandGroup(
    //     new WaitCommand(3.0), 
    //     new ParallelCommandGroup(
    //       new RunCommand(() -> m_kicker.runKicker(), m_kicker).withTimeout(0.4),
    //       new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1), m_topConveyor).withTimeout(0.38),
    //       new RunCommand(() -> m_backConveyor.setIntakeSpeed(1.0), m_backConveyor).withTimeout(0.4)
    //     ).andThen(new InstantCommand(() -> {
    //       m_kicker.stop();
    //       m_topConveyor.stop();
    //       m_backConveyor.stop();
    //     }, m_kicker, m_topConveyor)),
    //     new WaitCommand(1.6),
    //     new ParallelCommandGroup(
    //       new RunCommand(() -> m_kicker.runKicker(), m_kicker).withTimeout(0.5),
    //       new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1), m_topConveyor).withTimeout(0.46),
    //       new RunCommand(() -> m_frontConveyor.setIntakeSpeed(1.0), m_backConveyor).withTimeout(0.45)
    //     ).andThen(new InstantCommand(() -> {
    //       m_kicker.stop();
    //       m_topConveyor.stop();
    //       m_frontConveyor.stop();
    //     }, m_kicker, m_topConveyor, m_backConveyor),
    //     new WaitCommand(1.6),
    //     new ParallelCommandGroup(
    //       new RunCommand(() -> m_kicker.runKicker(), m_kicker).withTimeout(1.0),
    //       new RunCommand(() -> m_topConveyor.setIntakeSpeed(-1), m_topConveyor).withTimeout(1.08)
    //     ).andThen(new InstantCommand(() -> {
    //       m_kicker.stop();
    //       m_topConveyor.stop();
    //     }, m_kicker, m_topConveyor)),
    //     new RunCommand(() -> m_driveBase.arcadeDrive(-0.4, 0), m_driveBase).withTimeout(0.8))
    //     .andThen(new InstantCommand(() -> m_driveBase.arcadeDrive(0, 0), m_driveBase))
    //   )
    // )
    // .andThen(new InstantCommand(() -> m_shooter.setShooterPower(0, 0), m_shooter));

    // return shootBall;


    // RamseteCommand command = new RamseteCommand(
    //   m_autoTrajectory,
    //   m_driveBase::getRobotPose,
    //   new RamseteController(),
    //   m_driveBase.getDrivetrainFeedforward(),
    //   m_driveBase.getKinematics(),
    //   m_driveBase::getWheelSpeeds,
    //   m_driveBase.getLeftPidController(),
    //   m_driveBase.getRightPidController(),
    //   m_driveBase::setOutputVolts,
    //   m_driveBase
    // );

    // return command.andThen(() -> m_driveBase.arcadeDrive(0, 0));
  }
}
